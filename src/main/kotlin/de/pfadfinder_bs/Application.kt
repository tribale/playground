package de.pfadfinder_bs

import io.ktor.server.engine.*
import io.ktor.server.cio.*
import de.pfadfinder_bs.plugins.*

fun main() {
    embeddedServer(CIO, port = 8080, host = "0.0.0.0") {
        configureRouting()
        configureSecurity()
        configureMonitoring()
        configureHTTP()
        configureSockets()
    }.start(wait = true)
}
