FROM adoptopenjdk/openjdk11:jdk-11.0.11_9-alpine

ENV APPLICATION_USER ktor
RUN adduser -D -g '' $APPLICATION_USER \

 && mkdir /app \
 && chown -R $APPLICATION_USER /app

USER $APPLICATION_USER

COPY ./build/libs/tribale.jar /app/tribale.jar
WORKDIR /app

CMD [ \
    "java", \
    "-server", \
    "-jar", \
    "tribale.jar" \
    ]
