This project serves as collaborative playground for the tribale development team to try out some technologies befor integrating them to the app or to practice some git skills.

### Development

How to build and run:

```bash
./gradlew run
```

### Production

how to build and run the docker container
```bash
./gradlew build
docker-compose up
```
